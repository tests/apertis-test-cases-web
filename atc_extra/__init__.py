#!/usr/bin/env python3
import os
import pkg_resources
import shutil

from jinja2 import Environment, PackageLoader

PYTHON_PKGNAME='atc_extra'

def copy_files(directory, dirname, dirpath, msg):
    """
    Only copy files if destination directory (dst_dir) is not in the cwd.
    """
    dst_dir = os.path.join(os.path.realpath(directory), dirname)
    if not os.path.isdir(dst_dir):
        print("Creating directory", dst_dir)
        os.mkdir(dst_dir)

    print(msg, dst_dir)
    files = os.listdir(dirpath)
    for f in files:
        shutil.copy2(os.path.join(dirpath, f), dst_dir)


def main(output_path, releases):
    # Copy CSS
    css_dir = pkg_resources.resource_filename(PYTHON_PKGNAME, 'css/')
    copy_files(output_path, 'css/', css_dir, "Copying css style to")

    # Get template from environment and render it.
    env = Environment(loader=PackageLoader(PYTHON_PKGNAME, 'templates/'))
    template_values = { 'index_files': releases}
    data = env.get_template('menu.html').render(template_values)

    output_path = os.path.join(output_path, "index.html")
    with open(output_path, 'w') as index_html_file:
        index_html_file.write(data)

